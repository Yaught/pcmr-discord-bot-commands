/**
 * Pretty hacky and bad JavaScript. Sorry <3
 */
const editForm = document.getElementById("editForm");
const jsonOutput = document.getElementById("jsonPreview");
const commandSet = document.getElementById("commandSet");

if (!editForm || !jsonOutput) {
  throw new Error();
}

// Form input output
const parseFormValues = (form) => {
  const list = form.commandList.value
    .split("\n")
    .map((s) => s.trim().toLowerCase());
  const name = list[0].toUpperCase();
  const aliases = list.slice(1);
  const data = {
    name: form.name?.value,
    text: form.text?.value,
    image: form.image?.value || undefined,
    aliases,
  };
  
  return [name, data];
};

const handleFormInput = function () {
  const [name, data] = parseFormValues(editForm);
  const obj = JSON.stringify(data, null, 4);
  // JSON preview
  jsonOutput.innerHTML = `"${name}": ${obj}`;
  doEmbedPreview(data);
};

const doEmbedPreview = function (data) {
  // redirect relative paths
  let imgSrc = data.image;
  if (imgSrc?.startsWith("/")) {
      imgSrc = `https://gitlab.com/pcmasterrace/discord/pcmr-discord-bot-commands/-/raw/master${imgSrc}`;
  }
  const title =  document.querySelector("#embedTitle");
  const body =  document.querySelector("#embedBody");
  const imageEl =  document.querySelector("#embedImage");
  const imageContainer =  document.querySelector(".command_image");
  title.innerHTML = marked.parseInline(data.name);
  body.innerHTML = marked.parseInline(data.text);
  if (imgSrc) {
    imageEl.src = imgSrc;
    imageEl.style.display = "block";
  } else {
    imageEl.style.display = "none";
  }
};

editForm?.addEventListener("input", handleFormInput);
handleFormInput();

let _commandsCache = undefined; // cache
const getAllCommands = async function () {
  if (!_commandsCache) {
    const res = await fetch("commands.json");
    _commandsCache = await res.json();
  }
  return _commandsCache;
};

const loadCommandDataset = async function () {
  for (const commandName in await getAllCommands()) {
    const option = document.createElement("option");
    option.value = commandName;
    option.innerText = commandName.toLowerCase();
    commandSet?.appendChild(option);
  }
};
loadCommandDataset();

const getCommand = async function (key) {
  const commands = await getAllCommands();
  if (!(key in commands)) {
    alert(`Command ${key} not found.`);
    return;
  }
  return commands[key];
};

const handleLoadFormSubmit = async function (ev) {
  ev.preventDefault();
  const key = commandSet?.value.toUpperCase();
  const command = await getCommand(key);
  const aliasList = [key.toLowerCase(), ...(command.aliases ?? [])];
  editForm["name"].value = command.name ?? "";
  editForm["text"].value = command.text ?? "";
  editForm["commandList"].value = aliasList.join("\n");
  editForm["image"].value = command.image ?? "";
  handleFormInput();
  resizeTextAreas();
};
commandSet?.addEventListener("change", handleLoadFormSubmit);

const handleTextAreaInput = function() {
  // why tf doesn't the browser do this by default
  this.style.height = "";
  this.style.height = this.scrollHeight + "px";
}
const resizeTextAreas = () => {
  for (const textarea of document.querySelectorAll("textarea")) {
    textarea.addEventListener("input", handleTextAreaInput);
    handleTextAreaInput.apply(textarea);
  }
}
resizeTextAreas();